/* Simple test of thread create
 */

#include <stdio.h>
#include <stdlib.h>
#include <sthread.h>

void *thread_start(void *);

#define MAXTHREADS 10
int nThreads = 3;

int main(int argc, char **argv) {
  int n;
  int retVal;
  sthread_t child[MAXTHREADS];

  printf("Testing sthread_create, impl: %s\n",
	 (sthread_get_impl() == STHREAD_PTHREAD_IMPL) ? "pthread" : "user");

  if ( argv[1] ) nThreads = atoi(argv[1]);
  if ( nThreads > MAXTHREADS ) nThreads = MAXTHREADS;
  printf("Creating %d threads\n", nThreads);
  
  sthread_init();
  
  for ( n=0; n<nThreads; n++ ) {
    if ( (child[n] = sthread_create(thread_start, (void*)n, 1) ) == NULL) {
      printf("sthread_create failed\n");
      exit(1);
    }
  }
    
  for ( n=nThreads-1; n>=0; n-- ) {
    retVal = (int)sthread_join(child[n]);
    printf("main: joined with %d: %d\n", n, retVal);
  }

  return 0;
}

void *thread_start(void *arg) {
  printf("\tChild in thread_start, arg = %d\n", (int)arg);
  return (void*)-(int)arg;
}
