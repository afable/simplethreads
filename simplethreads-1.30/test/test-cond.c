/*
 * test-cond.c - Simple test of condition variables.
 *
 * Implements a producer/consumer with 2 consumer threads to
 * transfer 100 items.
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#include <sthread.h>

static const int max_transfer = 100;
static int transfered = 0;
static int waiting = 0;
static sthread_mutex_t mutex;
static sthread_cond_t avail_cond;

void *thread_start(void *);

int main(int argc, char **argv) {
  int sent, checks;
  
  printf("Testing sthread_cond_*, impl: %s\n",
	 (sthread_get_impl() == STHREAD_PTHREAD_IMPL) ? "pthread" : "user");

  sthread_init();

  mutex = sthread_mutex_init();
  avail_cond = sthread_cond_init();
  
  sthread_mutex_lock(mutex);
  
  if (sthread_create(thread_start, (void*)1, 0) == NULL) {
    printf("first sthread_create failed\n");
    exit(1);
  }
  if (sthread_create(thread_start, (void*)1, 0) == NULL) {
    printf("second sthread_create failed\n");
    exit(1);
  }

  assert(transfered == 0);
  
  /* This should let the other thread run at some point. */
  sthread_mutex_unlock(mutex);

  /* Send a bunch of things for the other threads to take */
  sent = 0;
  while (sent < max_transfer) {
    sthread_mutex_lock(mutex);
    waiting++;
    sent++;
    sthread_cond_signal(avail_cond);
    sthread_mutex_unlock(mutex);
    sthread_yield();
  }

  printf("Sent %d\n", sent);
  
  /* Now give the other threads 100 tries to get
   * them all across. We assume that's enough
   * for the sake of not running this test forever. */
  checks = 100;
  while (checks > 0) {
    sthread_mutex_lock(mutex);
    if (transfered != max_transfer)
      checks--;
    else {
      /* broadcast to let the consumers know we've
       * finished, so they can exit
       * (othrewise, they may still be holding the lock
       * when we try to free it below) */
      sthread_cond_broadcast(avail_cond);
      checks = -1;
    }
    sthread_mutex_unlock(mutex);
    sthread_yield();
  }
  
  if (checks == -1)
    printf("sthread_cond passed\n");
  else
    printf("*** sthread_cond failed\n");
  
  sthread_mutex_free(mutex);
  sthread_cond_free(avail_cond);
  return 0;
}

/* Consumer thread - remove items from the "buffer"
 * until they've all been transfered. */
void *thread_start(void *arg) {
  sthread_mutex_lock(mutex);

  while (transfered < max_transfer) {
    /* Wait for main() to put something up for us to take */
    while (transfered < max_transfer && waiting == 0) {
      sthread_cond_wait(avail_cond, mutex);

      /* This isn't part of using cond vars, but
       * helps the test fail quickly if they aren't
       * working properly: */
      sthread_mutex_unlock(mutex);
      sthread_yield();
      sthread_mutex_lock(mutex);
    }
    /* Either there is something waiting, or we've finished */
    
    if (waiting != 0) {
      /* Take it */
      waiting--;
      transfered++;
    }
  }
  
  sthread_mutex_unlock(mutex);
  
  return 0;
}
