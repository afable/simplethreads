/* Description: Implements a producer/consumer with
 * 		N producers and M consumer transfer
 *		Z items.
 *
 * test-preempt.c - Simple test of condition variables.
 *
 * Note: This file contains the test-burgers.c prod/cons
 *	simulation. A new test could not be added to the
 *	codebase (many errors appeared after re-configuring)
 *	so the program is inserted here as well as on its own.
 *	test-burgers.c can be compiled with.
 *
 * Change Log:
 * 2011-10-21	     Erik Afable, CSC 360 Ass#2b, University
 * 					of Victoria Fall 2011
 * 2002-04-15        rick
 *   - Initial version.
 *
 * Assumptions:
 *   - Yvonne said memory leaks will not be tested.
 *   - codebase did not let me add tests to its makefile, so
 *	I have included my test-burgers.c code into the
 *	test-preempt.c thats already in the test/ folder.
 */

#include <stdio.h>
#include <stdlib.h>
#include <sthread.h>

/* Global statics used by prods & cons threads */
static sthread_mutex_t mutex;
static sthread_cond_t burgers_ready;

/* Global variables used by prods & cons threads */
int max_burgers;
int profs;
int students;
int burger_id = 0;
int burgers_waiting;
int burgers_cooked;
int *p_cook_burger;
int *p_eat_burger;

/* Function prototypes */
void *cook(int **pp_cook_burger);
void *eat(int **pp_cook_burger);

int main(int argc, char *argv[])
{	
	/* Usage clause if less than 4 arguments are passed */
	if ( argc != 4 )
	{
		printf("\n usage:\t./test-preempt N M Z\n");
		printf("    N = Number of able-bodied profs cooking\n");
		printf("    M = Number of hungry students eating\n");
		printf("    Z = Maximum number of burgers to cook\n\n");
		printf(" example:\n");
		printf("\t./test-preempt 3 2 100\n");
		printf("\t(3 profs cook 100 burgers for 2 students)\n\n");
	}
	else
	{
		/* Initialize passed parameters */
		profs = atoi(argv[1]);
		students = atoi(argv[2]);
		max_burgers = atoi(argv[3]);

		/* Initialize global variables */
		burgers_waiting = 0;
		burgers_cooked = 0;

		/* Declare burgers array and index var */
		int burgers[max_burgers];
		int i;

		/* Initialize thread start_routine arguments */
		int **pp_cook_burger;
		int **pp_eat_burger;
		p_cook_burger = burgers;
		p_eat_burger = burgers;
		pp_cook_burger = &p_cook_burger;
		pp_eat_burger = &p_eat_burger;

		/* Initialize burgers array */
		for ( i = 0; i < max_burgers; ++i )
			burgers[i] = 0;

		/* Initialize sthread structs */
		sthread_init();
		mutex = sthread_mutex_init();
		burgers_ready = sthread_cond_init();

		printf("/* %i hungry student(s), %i able-bodied prof(s), %i burger(s) */\n",
							students, profs, max_burgers);

		/* Creating student and prof threads */
		for ( i = 0; i < students; ++i )
			if (sthread_create(eat, pp_eat_burger, 0) == NULL)
			{
				sprintf(stderr, "\tERROR: THREAD CREATE FAILED!!\n");
				exit(1);
			}

		for ( i = 0; i < profs; ++i )
			if (sthread_create(cook, pp_cook_burger, 0) == NULL)
			{
				sprintf(stderr, "\tERROR: THREAD CREATE FAILED!!\n");
				exit(1);
			}

		/* Cooking and eating burgers */
		while ( (burgers_cooked < max_burgers) || !(burgers_waiting == 0) )
			sthread_yield();

		printf("All done! %i burgers left!\n", burgers_waiting);
	}
	return 0;
}

/* @description:
 *	Produces to the burgers buffer whenever the mutex is available.
 *	Signals any waiting consumers when a burger is produced and
 *	broadcasts all waiting consumers if finished.
 * @args:
 *	int **pp_cook_burger --a pointer to the pointer used as an index
 *	  for traversing cooked burgers.
 * @returns:
 *	none.
 */
void *cook(int **pp_cook_burger)
{
	while ( (burgers_cooked < max_burgers) || !(burgers_waiting == 0) )
	{
		/* if we can hold the mutex, produce burgers */
		sthread_mutex_lock(mutex);

		if ( burgers_cooked < max_burgers )
		{
			**pp_cook_burger = burger_id++;
			printf("\tprof cooked burger %i!\n", **pp_cook_burger);
			++(*pp_cook_burger);
			++burgers_waiting;
			++burgers_cooked;
		}
		else
		{
			/* broadcast to let the consumers know we've
			* finished, so they can exit
			* (othrewise, they may still be holding the lock
			* when we try to free it below) */
			sthread_cond_broadcast(burgers_ready);
		}
		sthread_mutex_unlock(mutex);
		sthread_cond_signal(burgers_ready);
		sthread_yield();
	}

	return 0;
}

/* Consumer thread - remove items from the "buffer"
 * until they've all been transfered. */
/* @description:
 *	If there are waiting burgers, consumer consumes burgers. If
 *	there aren't any waiting burgers, then consumer waits on condVar
 *	burgers_ready until a burger is produced.
 * @args:
 *	int **pp_eat_burger --a pointer to the pointer used as an index
 *	  for traversing eaten burgers.
 * @returns:
 *	none.
 */
void *eat(int **pp_eat_burger)
{
	sthread_mutex_lock(mutex);

	while ( (burgers_cooked < max_burgers) || !(burgers_waiting == 0) )
	{
		/* Wait for profs to cook burgers for students */
		while ( (burgers_cooked < max_burgers) && (burgers_waiting == 0) )
		{
			printf("STUDENT WAITING FOR BURGERS!\n");
			sthread_cond_wait(burgers_ready, mutex);
		}

		/* Either there is something waiting, or we've finished */
		if ( burgers_waiting != 0 )
		{
			/* Eat some burgers */
			--burgers_waiting;
			printf("\tstudent ate burger %i...\n", **pp_eat_burger);
			++(*pp_eat_burger);
		}
	}

	sthread_mutex_unlock(mutex);
	return 0;
}


