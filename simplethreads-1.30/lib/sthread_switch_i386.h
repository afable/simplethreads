#include <config.h>

#ifndef __ASM__ /* in C mode */

void __attribute__((regparm(2))) Xsthread_switch(char **old_sp, char *new_sp);

/* Tell the stack-setup code how much space we expect
 * to be pushed on the stack. We push all 8 general
 * purpose registers on i386.
 */
#define STHREAD_CPU_SWITCH_REGISTERS 8

#else  /* in assembly mode */

.globl _old_sp
.globl _new_sp
.globl Xsthread_switch
.globl print_sp
.globl here

/* in C terms: void Xsthread_switch(void) */
Xsthread_switch:
/* Push register state onto our current (old) stack for 64 bit! */
       pushq %rax
       pushq %rcx
       pushq %rdx
       pushq %rbx
       pushq %rsi
       pushq %rdi
       pushq %rbp
       pushfq

movq %rsp, (%rdi)
     movq %rsi, %rsp
  //NEW WORLD IS NOW!

       popfq
       popq %rbp
       popq %rdi
       popq %rsi
       popq %rbx
       popq %rdx
       popq %rcx
       popq %rax

       ret

#endif /* __ASM__ */    
