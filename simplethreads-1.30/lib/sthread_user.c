/* Description: Simplethreads Instructional Thread Package.
 * Implements the sthread API using user-level threads.
 *
 * Change Log:
 *
 * 2011-10-21	     Erik Afable
 *   - Implemented the following 3 files:
 *		- /lib/sthread_user.c
 *		- /test/test-preempt.c
 *
 * 2002-04-15        rick
 *   - Initial version.
 *
 * Assumptions:
 *   - memory leaks not tested.
 *   - codebase did not let me add tests to its makefile, so
 *	  I have included my test-burgers.c code into the
 *	  test-preempt.c thats already in the test/ folder.
 */

#include <stdlib.h>
#include <assert.h>
#include <stdio.h>
#include <string.h>
#include <sthread.h>
#include <sthread_queue.h>
#include <sthread_user.h>
#include <sthread_ctx.h>
#include <sthread_user.h>

//#define DEBUG
#define YES 1
#define NO 0
#define MAIN_ARGS (void*)-1

/* typedef struct _sthread *sthread_t; */
struct _sthread
{
	sthread_ctx_t *ctx;
	void *function;
	void *args;
	int id;
	int joinable;

	sthread_queue_t joinQ;
};

/* IDs assigned to structs */
static int g_thread_id = -1; // first thread main is -1
static int g_mutex_id = 0; // first mutex is 0
static int g_cond_id = 99; // first condVar is 99

static sthread_queue_t g_readyQ = NULL;
static sthread_queue_t g_deadQ = NULL;

/* All-purpose threads */
static sthread_t g_current_thread;
static sthread_t g_dummy_thread;
static sthread_t temp;
static sthread_t main_thread;

/* All-purpose structs */
static sthread_ctx_t *main_ctx;
static sthread_ctx_t *dummy_ctx;
static sthread_ctx_t *temp_ctx;
static sthread_mutex_t dummy_mutex;
static sthread_cond_t dummy_cond;

/* Function Prototypes */
static  void dispatcher();

/*********************************************************************/
/* Part 1: Creating and Scheduling Threads                           */
/*********************************************************************/

/* @description:
 *	initializes global queues and a TCB for main thread.
 * @args:
 *	none.
 * @returns:
 *	none.
 */
void sthread_user_init(void)
{
	/* setup ready, dead queues */
	g_readyQ = sthread_new_queue();
	g_deadQ = sthread_new_queue();

	/* create a thread for main and initialize its fields */
	g_dummy_thread = (sthread_t)malloc(sizeof(struct _sthread));
	g_dummy_thread->ctx = sthread_new_ctx(dispatcher);
	g_dummy_thread->args = MAIN_ARGS;
	g_dummy_thread->id = g_thread_id++;
	g_dummy_thread->joinable = YES;
	g_dummy_thread->joinQ = sthread_new_queue();

	/* current thread at init is main */
	g_current_thread = g_dummy_thread;
	main_thread = g_dummy_thread;
}

/* @description:
 *	initializes a TCB for a new thread and queues it to the readyQ.
 * @args:
 * 	start_routine --the routine the new thread executes.
 *	void *arg --argument for the start_routine.
 *	int joinable --1 for thread joinable, 0 otherwise.
 * @returns:
 *	a pointer to the new thread's TCB.
 */
sthread_t sthread_user_create(sthread_start_func_t start_routine,
					void *arg, int joinable)
{
	/* create new thread and initialize its fields */
	sthread_t g_dummy_thread = (sthread_t)malloc(sizeof(struct _sthread));
	g_dummy_thread->function = start_routine;
	g_dummy_thread->args = arg;
	g_dummy_thread->id = g_thread_id++;
	g_dummy_thread->joinable = joinable;
	g_dummy_thread->joinQ = sthread_new_queue();

	/* get ctx to point to dispatcher that will run the thread's
	 * function on ctx switch */
	g_dummy_thread->ctx = sthread_new_ctx(dispatcher);

	/* put new thread on the ready queue */
	sthread_enqueue(g_readyQ, g_dummy_thread);

	/* return a pointer to newly created thread */
	return g_dummy_thread;
}

/* @description:
 *	Called from dispatcher when a thread is to exit it's routine.
 *	All threads on the exiting thread's joinQ are enqueued to
 *	g_readyQ.
 * @args:
 * 	void *return_value --the thread's start_routine's return value.
 * @returns:
 *	none.
 */
void sthread_user_exit(void *return_value)
{
	/* check and enqueue all threads on exiting thread's joinQ
	 * to g_readyQ */
	while ( !sthread_queue_is_empty(g_current_thread->joinQ) )
		sthread_enqueue(g_readyQ, sthread_dequeue(g_current_thread->joinQ));

	/* g_readyQ should always at least have main in there but... */	
	if ( !sthread_queue_is_empty(g_readyQ) )
	{
		/* dequeue next thread from g_readyQ, enqueue exiting thread
		 * to g_deadQ, and switch the two threads */
		temp = g_current_thread;
		g_current_thread = sthread_dequeue(g_readyQ);
		sthread_enqueue(g_deadQ, temp);
		sthread_switch(temp->ctx, g_current_thread->ctx);
	}
	
	/* this should never be the case since main is the one that exits but... */
	printf("YOU SHOULD NEVER SEE THIS SINCE main() SHOULD ALWAYS EXIT LAST.\n");
	printf("YOU SHOULD NEVER SEE THIS SINCE main() SHOULD ALWAYS EXIT LAST.\n");
	printf("YOU SHOULD NEVER SEE THIS SINCE main() SHOULD ALWAYS EXIT LAST.\n");
	printf("YOU SHOULD NEVER SEE THIS SINCE main() SHOULD ALWAYS EXIT LAST.\n");
	exit(return_value);
}

/* @description:
 *	Calling thread will join another thread t and wait until t has
 *	finished executing before continuing to execute. So add joining
 *	thread to t's joinQ.
 * @args:
 * 	sthread_t t --the thread being joined to.
 * @returns:
 *	none.
 */
void* sthread_user_join(sthread_t t)
{
	if ( t == NULL )
		return (void*)-1;

	/* join fails if join called when nothing left in ready queue */
	if ( sthread_queue_is_empty(g_readyQ) || !t->joinable )
		return (void*)-1;

	/* enqueue calling thread to callee's joinQ */
	sthread_enqueue(t->joinQ, g_current_thread);

	/* context switch from calling thread to new thread off stack */
	temp = g_current_thread;
	g_current_thread = sthread_dequeue(g_readyQ);
	sthread_switch(temp->ctx, g_current_thread->ctx);

	return 0;
}

/* @description:
 *	Current thread will yield execution and let next thread from
 *	g_readyQ execute. Yielded thread queued to g_readyQ.
 * @args:
 *	none.
 * @returns:
 *	none.
 */
void sthread_user_yield(void)
{
	/* if no more threads left to yield to, return to the yield call */
	if ( sthread_queue_is_empty(g_readyQ) )
		return;

	/* dequeue next thread from and current thread to readyQ and switch */
	temp = g_current_thread;
	g_current_thread = sthread_dequeue(g_readyQ);
	sthread_enqueue(g_readyQ, temp);
	sthread_switch(temp->ctx, g_current_thread->ctx);
}

/* NEW PART 1 FUNCTIONS BELOW */

/* @description:
 *	dispatcher will run the passed in function during a ctx switch.
 * @args:
 *	none.
 * @returns:
 *	none.
 */
void dispatcher()
{
	sthread_start_func_t func = g_current_thread->function; // func = function
	(func)(g_current_thread->args); // call func with argument args

	/* return from running a thread's function here */
	sthread_user_exit((void*)g_current_thread->id);
}

/*********************************************************************/
/* Part 2: Synchronization Primitives                                */
/*********************************************************************/

/* typedef struct _sthread_mutex *sthread_mutex_t; */
struct _sthread_mutex
{
	int locked;
	sthread_queue_t mutexQ;
	int id;
};

/* @description:
 *	initializes a new mutex and its attributes.
 * @args:
 *	none.
 * @returns:
 *	a pointer to the initialized mutex.
 */
sthread_mutex_t sthread_user_mutex_init()
{
	dummy_mutex = (sthread_mutex_t)malloc(sizeof(struct _sthread_mutex));
	dummy_mutex->locked = NO;
	dummy_mutex->mutexQ = sthread_new_queue();
	dummy_mutex->id = g_mutex_id++;
	return dummy_mutex;
}

/* @description:
 *	queues all TCBs on mutexQ to readyQ and frees mutex and mutexQ memory.
 * @args:
 *	none.
 * @returns:
 *	none.
 */
void sthread_user_mutex_free(sthread_mutex_t lock)
{
	if ( lock == NULL )
		return;

	while ( !sthread_queue_is_empty(lock->mutexQ) )
		sthread_enqueue(g_readyQ, sthread_dequeue(lock->mutexQ));
	lock->locked = NO;
	free(lock->mutexQ);
	free(lock);
}

/* @description:
 *	If mutex unlocked, lock it. If mutex locked, put current thread
 *	on mutexQ and ctx switch to next thread from readyQ.
 * @args:
 *	sthread_mutex_t lock --the mutex to lock.
 * @returns:
 *	none.
 */
void sthread_user_mutex_lock(sthread_mutex_t lock)
{
	if ( lock == NULL )
	{
		sprintf(stderr, "\tError: thread %i locking freed mutex\n",
						g_current_thread->id);
		return;
	}

	/* case 1: if mutex unlocked trying to lock/grab it */
	if ( !(lock->locked) )
		lock->locked = YES;

	/* case 2: if mutex locked, put thread trying to lock/grab
	 * it on this mutex's mutexQ and ctx switch to next thread
	 * from readyQ */
	else
	{
		sthread_enqueue(lock->mutexQ, g_current_thread);
		temp = g_current_thread;
		g_current_thread = sthread_dequeue(g_readyQ);
		sthread_switch(temp->ctx, g_current_thread->ctx);
	}
}

/* @description:
 *	Move everything from mutex's mutexQ to readyQ and unlock
 *	the mutex.
 * @args:
 *	sthread_mutex_t lock --the mutex to unlock.
 * @returns:
 *	none.
 */
void sthread_user_mutex_unlock(sthread_mutex_t lock)
{
	if ( lock == NULL )
	{
		sprintf(stderr, "\tError: thread %i unlocking freed mutex\n",
						g_current_thread->id);
		return;
	}

	while ( !sthread_queue_is_empty(lock->mutexQ) )
		sthread_enqueue(g_readyQ, sthread_dequeue(lock->mutexQ));

	lock->locked = NO;
}

/* typedef struct _sthread_cond *sthread_cond_t; */
struct _sthread_cond
{
	sthread_queue_t condQ;
	int id;
};

/* @description:
 *	Initialize condVar struct and condQ.
 * @args:
 *	none.
 * @returns:
 *	a pointer to the initialized condVar.
 */
sthread_cond_t sthread_user_cond_init(void)
{
	dummy_cond = (sthread_cond_t)malloc(sizeof(struct _sthread_cond));
	dummy_cond->condQ = sthread_new_queue();
	dummy_cond->id = g_cond_id++;
	return dummy_cond;
}

/* @description:
 *	Free condQ and condVar struct.
 * @args:
 *	sthread_cond_t cond, the condVar to free.
 * @returns:
 *	none.
 */
void sthread_user_cond_free(sthread_cond_t cond)
{
	free(cond->condQ);
	free(cond);
}

/* @description:
 *	Signal and condVar, so take 1 TCB from condQ and
 *	queue it to g_readyQ.
 * @args:
 *	sthread_cond_t cond, the condition variable to signal.
 * @returns:
 *	none.
 */
void sthread_user_cond_signal(sthread_cond_t cond)
{
	if ( cond == NULL )
		return;

	if ( !sthread_queue_is_empty(cond->condQ) )
		sthread_enqueue(g_readyQ, sthread_dequeue(cond->condQ));
	else
		sprintf(stderr, "\tError: condVar %i's condQ empty!\n", cond->id);
}

/* @description:
 *	Take all TCBs from condQ and put on g_readyQ.
 * @args:
 *	sthread_cond_t cond, the condition variable to broadcast.
 * @returns:
 *	none.
 */
void sthread_user_cond_broadcast(sthread_cond_t cond)
{
	if ( cond == NULL )
		return;

	while ( !sthread_queue_is_empty(cond->condQ) )
		sthread_enqueue(g_readyQ, sthread_dequeue(cond->condQ));
}

/* @description:
 *	Unlock the mutex, put the waiting thread on condQ and ctx
 *	switch to next thread off g_readyQ and re-acquire lock before
 *	returning. Wait for a producer to signal a consumer.
 * @args:
 *	sthread_cond_t cond, the condition variable to wait on.
 *	sthread_mutex_t lock, the mutex lock to wait on (with condition).
 * @returns:
 *	none.
 */
void sthread_user_cond_wait(sthread_cond_t cond, sthread_mutex_t lock)
{
	if ( cond && lock )
	{
		sthread_user_mutex_unlock(lock);
		sthread_enqueue(cond->condQ, g_current_thread);
		temp = g_current_thread;
		g_current_thread = sthread_dequeue(g_readyQ);
		sthread_switch(temp->ctx, g_current_thread->ctx);
		sthread_user_mutex_lock(lock);
	}
}


