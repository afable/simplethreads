Erik Afable

Last updated: October 22, 2011

--------------------------------------------------------------------------------
How to test code base
--------------------------------------------------------------------------------
1. Extract code base:
		tar –xvf simplethreads-1.30.tar.gz

2. Configure the build from the ./simplethreads-­‐1.30 directory:
		./configure --build=i386

3. From ./simplethreads--1.30, run "make" to make build and then "make check" to test it.

4. Note that I have only implemented the /lib/sthread_user.c file which works with the tests in /simplethreads-­‐1.30/test/. Specific tests (such as test-preempt.c that requires input) can be run from /simplethreads-­‐1.30/test/.

--------------------------------------------------------------------------------
Explanation and Justification of Simple Thread Library sthread.h Implementation

Outline of Report and Changes to Original Library:
--------------------------------------------------------------------------------

In this report, I support my design of the submitted sthread library. From the original simplethreads-1.30.tar.gz, I modified two files: /lib/sthread_user.c and /test/test-preempt.c. I have also added one new file: /test/test-burgers.c (attached since I could not autoreconf the code base).

--------------------------------------------------------------------------------
Assumptions:
--------------------------------------------------------------------------------

The library assumes threads are preemptive.


Profs Cook Burgers for Students Program:

The modified test program (/test/test-preempt) contains the required multi-threaded consumer/producer simulation (test-burgers.c). The thread, mutex, and condition variable designs are explained below.

--------------------------------------------------------------------------------
Design Details:
--------------------------------------------------------------------------------

  Simple Thread Design

I designed my simple threads to be scheduled on a first come first serve basis, i.e. the first thread scheduled will be the next thread to execute from a "ready" queue. New threads that are created also follow this scheduling policy. The threads are non-preemptive and thus, will only stop execution when calling yield. A thread T1 can join a "joinable" thread T2. T1 will then wait until T2 completes execution before being rescheduled (note this does not mean that T1 will execute immediately after T2 completes). Exiting threads are added to a "dead" queue that can be used for memory release (not yet implemented). Execution will be passed from an exiting thread to the next "ready" thread.

  Mutex Design

Mutexs hold a locked and unlocked state. A thread attempting to lock an already locked mutex will wait on a mutex until it is unlocked. The thread can then re-attempt to lock the mutex. If a mutex is unlocked, the attempting thread will lock it (and can modify the critical section of some code).

  Condition Variable Design

A thread in its critical section can wait on a condition variable for a mutex. Producers can then signal that condition variable and let a consumer be rescheduled to consume. Producers can also broadcast to all consumers waiting on a condition variable to signal the producer has finished.

--------------------------------------------------------------------------------
Errors:
--------------------------------------------------------------------------------

The library does not pass valgrind and has memory leaks. The test-burgers.c program was not successfully added into the library (errors posted on piazza). Instead, the content of test-burgers.c is included in the already available test-preempt.c file. This test can be run from the top-level directory by first executing "make" and then "make check".

--------------------------------------------------------------------------------
Justification for Design:
--------------------------------------------------------------------------------

Thread, mutex, and condition variable functions are explained in the Operating Systems In Depth textbook by T. W. Doeppner. Feedback was also given from various peers (I have included a list of peers in sthread_user.h) for design implemenation.






